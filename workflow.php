<?php
require_once(__DIR__ . '/crest.php');

$deal = $_GET['deal'];
$document = $_GET['document'];

//$deal = 24045;
//$document = 'Contrato de procedimiento en english sin CCA';
$params = [];

$checkFields = false;
if ($document == 'Contrato de Design Plastic en ingles') {
  $checkFields = true;
}
if ($document == 'Contrato de Design Plastic en espanol') {
  $checkFields = true;
}
if ($document == 'Contrato de Daso Plastic en espanol') {
  $checkFields = true;
}
if ($document == 'Contrato de Daso Plastic en ingles') {
  $checkFields = true;
}

if ($checkFields) {
  $products = CRest::call(
    'crm.deal.fields',
    ['id' => $deal]
  );
  $products = CRest::call(
    'crm.deal.productrows.get',
    ['id' => $deal]
  );
  $products = $products['result'];
  $index = 0;
  $bruto = 0;
  $liquid = 0;
  $discountSum = 0;
  foreach ($products as $product) {
    $discount = (int) $product['DISCOUNT_SUM'];
    $price = (int) $product["PRICE_BRUTTO"];
    $quantity = (int) $product["QUANTITY"];
    $discountSum = ($discount * $quantity)  + $discountSum;
    $discount = $discount * $quantity;
    $liquid = ($price * $quantity) + $liquid;
    $price = $price * $quantity;
    $bruto = ($price - $discount) + $bruto;
    $index++;

    // actualizar los productos en los campos
    if ($index == 1) {
      $arr1 =
        [
          'UF_CRM_1719532169322' =>  $product['PRODUCT_NAME'],
          'UF_CRM_1719929605' => '$' . $product['PRICE_BRUTTO'],
          'UF_CRM_1719532265430' => $product['QUANTITY'],
          'UF_CRM_1719532322254' => $product['TAX_RATE']
        ];
      $params = array_merge($params, $arr1);
    }
    if ($index == 2) {
      $arr2 =
        [
          'UF_CRM_1719532346320' =>  $product['PRODUCT_NAME'],
          'UF_CRM_1719929622' => '$' . $product['PRICE_BRUTTO'],
          'UF_CRM_1719532397977' => $product['QUANTITY'],
          'UF_CRM_1719532414346' => $product['TAX_RATE']
        ];
      $params = array_merge($params, $arr2);
    }
    if ($index == 3) {
      $arr3 =
        [
          'UF_CRM_1719592642117' =>  $product['PRODUCT_NAME'],
          'UF_CRM_1719929636' => '$' . $product['PRICE_BRUTTO'],
          'UF_CRM_1719592693955' => $product['QUANTITY'],
          'UF_CRM_1719592716676' => $product['TAX_RATE']
        ];
      $params = array_merge($params, $arr3);
    }
    if ($index == 4) {
      $arr4 =
        [
          'UF_CRM_1719863955' =>  $product['PRODUCT_NAME'],
          'UF_CRM_1719929654' => '$' . $product['PRICE_BRUTTO'],
          'UF_CRM_1719592778900' => $product['QUANTITY'],
          'UF_CRM_1719592801627' => $product['TAX_RATE']
        ];
      $params = array_merge($params, $arr4);
    }
    if ($index == 5) {
      $arr5 =
        [
          'UF_CRM_1719592819221' =>  $product['PRODUCT_NAME'],
          'UF_CRM_1719929718' => '$' . $product['PRICE_BRUTTO'],
          'UF_CRM_1719595600978' => $product['QUANTITY'],
          'UF_CRM_1719595618087' => $product['TAX_RATE']
        ];
      $params = array_merge($params, $arr5);
    }
    if ($index == 6) {
      $arr6 =
        [
          'UF_CRM_1719595535987' =>  $product['PRODUCT_NAME'],
          'UF_CRM_1719930534' => '$' . $product['PRICE_BRUTTO'],
          'UF_CRM_1719595600978' => $product['QUANTITY'],
          'UF_CRM_1719595618087' => $product['TAX_RATE']
        ];
      $params = array_merge($params, $arr6);
    }
    if ($index == 7) {
      $arr7 =
        [
          'UF_CRM_1719595669875' =>  $product['PRODUCT_NAME'],
          'UF_CRM_1719929733' => '$' . $product['PRICE_BRUTTO'],
          'UF_CRM_1719595708379' => $product['QUANTITY'],
          'UF_CRM_1719595736815' => $product['TAX_RATE']
        ];
      $params = array_merge($params, $arr7);
    }
    if ($index == 8) {
      $arr8 =
        [
          'UF_CRM_1727789358' =>  $product['PRODUCT_NAME'],
          'UF_CRM_1727789381' => '$' . $product['PRICE_BRUTTO'],
          'UF_CRM_1727789402' => $product['QUANTITY'],
          'UF_CRM_1727789418' => $product['TAX_RATE']
        ];
      $params = array_merge($params, $arr8);
    }
    if ($index == 9) {
      $arr9 =
        [
          'UF_CRM_1727789461' =>  $product['PRODUCT_NAME'],
          'UF_CRM_1727789477' => '$' . $product['PRICE_BRUTTO'],
          'UF_CRM_1727789494' => $product['QUANTITY'],
          'UF_CRM_1727789516' => $product['TAX_RATE']
        ];
      $params = array_merge($params, $arr9);
    }
    if ($index == 10) {
      $arr10 =
        [
          'UF_CRM_1727790137' =>  $product['PRODUCT_NAME'],
          'UF_CRM_1727790149' => '$' . $product['PRICE_BRUTTO'],
          'UF_CRM_1727790162' => $product['QUANTITY'],
          'UF_CRM_1727790174' => $product['TAX_RATE']
        ];
      $params = array_merge($params, $arr10);
    }
  }
}
if ($document == 'Contrato de Daso Plastic en ingles') {
  $contract1 = [
    'UF_CRM_1717090896' => 1954,
    'UF_CRM_1719847627579' => $liquid,
    'UF_CRM_1719929896' => $bruto,
    'UF_CRM_1719929882' => $discountSum
  ];
  $params = array_merge($params, $contract1);
}
if ($document == 'Contrato de Daso Plastic en espanol') {
  $contract1 = [
    'UF_CRM_1717090896' => 1998,
    'UF_CRM_1719847627579' => $liquid,
    'UF_CRM_1719929896' => $bruto,
    'UF_CRM_1719929882' => $discountSum
  ];
  $params = array_merge($params, $contract1);
}
if ($document == 'Contrato de Design Plastic en ingles') {
  $contract1 = [
    'UF_CRM_1717090896' => 1997,
    'UF_CRM_1719847627579' => $liquid,
    'UF_CRM_1719929896' => $bruto,
    'UF_CRM_1719929882' => $discountSum
  ];
  $params = array_merge($params, $contract1);
}
if ($document == 'Contrato de Design Plastic en espanol') {
  $contract1 = [
    'UF_CRM_1717090896' => 1999,
    'UF_CRM_1719847627579' => $liquid,
    'UF_CRM_1719929896' => $bruto,
    'UF_CRM_1719929882' => $discountSum
  ];
  $params = array_merge($params, $contract1);
}

if ($document == 'Apartments and transportation rules') {
  $contract4 = [
    'UF_CRM_1717090896' => 1959,
  ];
  $params = array_merge($params, $contract4);
}
if ($document == 'HIPAA Notice of Privacy Practices') {
  $contract4 = [
    'UF_CRM_1717090896' => 1960,
  ];
  $params = array_merge($params, $contract4);
}
$update = CRest::call(
  'crm.deal.update',
  [
    'id' => $deal,
    'fields' => $params
  ]
);
sleep(6);
$update = CRest::call(
  'crm.deal.update',
  [
    'id' => $deal,
    'fields' => array(
      'UF_CRM_1717090896' =>  '',
    )
  ]
);
