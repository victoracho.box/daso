<?php
require_once(__DIR__ . '/crest.php');
// ig portugal = PROCESSED 
// instagram = UC_IQEW2U
// a whatsapp = UC_ZCTIAB 

function report($stage)
{
  $results = [];
  $next = '0';
  do {
    $lead = CRest::call(
      'crm.lead.list',
      [
        'filter' => ['STATUS_ID' => $stage],
        'start' => $next,
        'select' => ['ID', 'PHONE', 'CONTACT_ID', 'HAS_PHONE', 'TITLE'],
      ],
    );
    $next = null;
    if (isset($lead['next'])) {
      $next = $lead['next'];
    }
    $results = array_merge($results, $lead['result']);
  } while ($next != null);

  foreach ($results as $result) {
    echo '<br>';
    print_r($result['PHONE']);
    echo '<br>';
  }
  echo count($results);
}
report('UC_ZCTIAB');
