<?php
require_once(__DIR__ . '/crest.php');
// lead es UC_FH5727 
// pool es UC_LSS7ZV
// wazup es UC_A7U9SN
// test es UC_TX3D8B
// test 2 es UC_BGL8OP
// duplicados es 9
// interested in promo es UC_A7U9SN
// comment pending contact es UC_Y28S31
if (!empty($_REQUEST['auth']['application_token']) && $_REQUEST['auth']['application_token'] == '4q6vk3rz1as4ynkuepr49ciczh8dopdk') {

  // logica para los duplicados
  if ($_REQUEST['event'] == 'ONCRMLEADADD') {
    $lead = crest::call(
      'crm.lead.get',
      [
        'id' => $_REQUEST['data']['FIELDS']['ID']
      ],
    );

    $lead = $lead['result'];
    $results = [];

    // Si tiene contacto 
    $contact = null;
    if ($lead['CONTACT_ID']) {
      $contact = crest::call(
        'crm.contact.get',
        [
          'id' => $lead['CONTACT_ID']
        ],
      );
      //  si tiene telefono
      $contact = $contact['result'];
      if ($contact['HAS_PHONE'] == 'Y') {
        $contactPhones = $contact['PHONE'];
        // recorro los telefonos del contacto, si no tienen +1 se le agrega
        foreach ($contactPhones as $phon) {
          if ($lead['STATUS_ID'] == 'UC_H334TY') {
            if (strpos($phon['VALUE'], '+1') === false) {
              $done = CRest::call(
                'crm.contact.update',
                [
                  'id' => $contact['ID'],
                  'fields' => [
                    'PHONE' => [
                      [
                        "ID" => $phon['ID'],
                        "VALUE" => '+1 ' . $phon['VALUE'],
                        "VALUE_TYPE" => $phon['VALUE_TYPE'],
                        "TYPE_ID" => $phon['TYPE_ID']
                      ]
                    ]
                  ]
                ]
              );
            }
          }
        }
      }
    }
    // si el lead tiene celular
    if ($lead['HAS_PHONE'] == 'Y') {
      $phones = $lead['PHONE'];
      foreach ($phones as $phone) {
        // modificamos el telefono
        if (strpos($phone['VALUE'], '+1') === false) {
          $done = CRest::call(
            'crm.lead.update',
            [
              'id' => $_REQUEST['data']['FIELDS']['ID'],
              'fields' => [
                'PHONE' => [
                  array(
                    "ID" => $phone['ID'],
                    "VALUE" => '+1 ' . $phone['VALUE'],
                    "VALUE_TYPE" => $phone['VALUE_TYPE'],
                    "TYPE_ID" => $phone['TYPE_ID']
                  )
                ]
              ]
            ]
          );
        }

        // por cada celular, llamo al api para verificar duplicados
        $duplicates = CRest::call(
          'crm.duplicate.findbycomm',
          [
            'entity_type' => "LEAD",
            'type' => "PHONE",
            'values' => array(
              $phone['VALUE']
            )
          ],
        );
        $duplicates = $duplicates['result'];
        if (isset($duplicates['LEAD'])) {
          $duplicates = $duplicates['LEAD'];
          $results = array_merge($results, $duplicates);
        }
      }
    }
    if ($lead['HAS_EMAIL'] == 'Y') {
      $emails = $lead['EMAIL'];
      // por cada email se llama al api para verificar duplicados
      foreach ($emails as $email) {
        $duplicates = CRest::call(
          'crm.duplicate.findbycomm',
          [
            'entity_type' => "LEAD",
            'type' => "EMAIL",
            'values' =>  array($email['VALUE'])
          ],
        );
        $duplicates = $duplicates['result'];
        if (isset($duplicates['LEAD'])) {
          $duplicates = $duplicates['LEAD'];
          $results = array_merge($results, $duplicates);
        }
      }
    }

    $results = array_diff($results, [$lead['ID']]);
    $results = array_unique($results);
    if (!empty($results)) {
      // se notifica que el lead ya existe 
      // cambiar el id del webhook
      $event = crest::call(
        'bizproc.workflow.start',
        [
          'TEMPLATE_ID' => 108,
          'DOCUMENT_ID' => ['crm', 'CCrmDocumentLead', 'LEAD_' . $lead['ID']]
        ],
      );
      // $url = 'https://btx.dds.miami/rest/10476/1mewdlanh4kgrnos/bizproc.workflow.start?TEMPLATE_ID=204&DOCUMENT_ID[]=crm&DOCUMENT_ID[]=CCrmDocumentLead&DOCUMENT_ID[]=LEAD_' . $lead['ID'];
      // si tiene duplicados, se le agrega (con duplicados) 
      // y se mueve a la columna duplicados 
      $rest = CRest::call(
        'crm.lead.update',
        [
          'id' => $_REQUEST['data']['FIELDS']['ID'],
          'fields' => [
            'TITLE' => $lead['TITLE'] . ' (con duplicados)',
            'STATUS_ID' => 'UC_HIDHQD',
          ]
        ],
      );
    }
    // si simplemente no tiene duplicados, se asigna a system user como usuario por defecto
    /* if (empty($results)) { */
    /*   $done = CRest::call( */
    /*     'crm.lead.update', */
    /*     [ */
    /*       'id' => $_REQUEST['data']['FIELDS']['ID'], */
    /*       'fields' => [ */
    /*         'ASSIGNED_BY_ID' => 11664 */
    /*       ] */
    /*     ] */
    /*   ); */
    /* } */
  }
  // asigno como responsable a elizabeth
  /* if ($_REQUEST['event'] == 'ONCRMCONTACTADD') { */
  /*   $lead = crest::call( */
  /*     'crm.contact.get', */
  /*     [ */
  /*       'id' => $_REQUEST['data']['FIELDS']['ID'] */
  /*     ], */
  /*   ); */
  /*   $updateLead = CRest::call( */
  /*     'crm.lead.update', */
  /*     [ */
  /*       'id' => $lead['result']['LEAD_ID'], */
  /*       'fields' => [ */
  /*         'ASSIGNED_BY_ID' => 7 */
  /*       ] */
  /*     ] */
  /*   ); */
  /* } */
}
