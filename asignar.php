<?php
// UC_HIDHQD es duplicados 

require_once(__DIR__ . '/crest.php');
$lead = crest::call(
  'crm.deal.productrows.get',
  [
    'id' => 22446
  ],
);

return $lead;

$next = '0';
$results = [];
do {
  $leads = CRest::call(
    'crm.lead.list',
    [
      'filter' => ['STATUS_ID' => 'UC_HIDHQD'],
      'start' => $next,
      'select' => ['ID', 'PHONE', 'CONTACT_ID', 'HAS_PHONE'],
    ],
  );
  // pasamos al siguiente batch, hasta que no haya mas resultados
  $next = null;
  if (isset($leads['next'])) {
    $next = $leads['next'];
  }
  $results = array_merge($results, $leads['result']);
} while ($next != null);

foreach ($results as $result) {
  $res = CRest::call(
    'crm.lead.update',
    [
      'id' => $result['ID'],
      'fields' => [
        'ASSIGNED_BY_ID' => 7
      ]
    ]
  );
}
